const _subscribeHook = async function (z, bundle, type) {
    const data = {
        targetUrl: bundle.targetUrl,
        event: type,
        zapId: bundle.meta.zap.id,
        zapierAccountId: 'zapierAccountId'
    };
    // bundle.action = '_subscribeHook'; _postLog(z, bundle);
    z.console.log(`zapId is ${bundle.meta.zap.id} | zapier.bundle.authData is ${JSON.stringify(bundle.authData)}`);
    let webHookUrl = '';
    switch (_checkPortalVersion(bundle.authData.domain)) {
        case 'autoCoding':
            webHookUrl = `https://${bundle.authData.domain}/api/livechat/integrationZapier/webhooks`;
            break;
        case 'xversion':
            let apiDomain = _getApiDomain(bundle.authData.domain);
            webHookUrl = `https://${apiDomain}/api/v3/livechat/webhooks`;
            break;
        case 'prex':
            break;
    }
    const options = {
        url: webHookUrl,
        method: 'POST',
        body: data,
        headers: {
            'Content-Type': 'application/json'
        }
    };

    return z
        .request(options)
        .then((response) => {
            if (response.status < 200 && response.status >= 300) {
                if (response.json) {
                    throw new Error(response.json.Message || response.json.ErrorMessage);
                } else {
                    throw new Error('SubscribeHook error.');
                }
            }
            return response.json;
        });
};

const _unsubscribeHook = async function (z, bundle) {
    //  bundle.action = "_unsubscribeHook";   _postLog(z,bundle);

    const hookId = bundle.subscribeData.id;
    let webHookUrl = '';
    switch (_checkPortalVersion(bundle.authData.domain)) {
        case 'autoCoding':
            webHookUrl = `https://${bundle.authData.domain}/api/livechat//integrationZapier/webhooks${hookId}`;
            break;
        case 'xversion':
          let apiDomain = _getApiDomain(bundle.authData.domain)
            webHookUrl = `https://${apiDomain}/api/v3/livechat/webhooks${hookId}`;
            break;
        case 'prex':
            break;
    }
    const options = {
        url: webHookUrl,
        method: 'DELETE'
    };

    return z
        .request(options)
        .then((response) => {
            if (response.status != 200) {
                if (response.json) {
                    throw new Error(response.json.Message || response.json.ErrorMessage);
                } else {
                    throw new Error('UnSubscribeHook error.');
                }
            }
            return response.json;
        });
};

const _copyJsonObject = (json) => {
    return JSON.parse(JSON.stringify(json));
};

const _postLog = (z, json) => {
    z
        .request({
            method: 'POST',
            url: 'https://hooks.zapier.com/hooks/catch/3966530/ed563w/',
            body: json,
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then((r) => {});
};

const _checkUrl = (z, url) => {
    const promise = z.request({method: 'HEAD', url: url});
    return promise
        .then((response) => {
            return response.status;
        })
        .catch((reason) => {
            return 404;
        });
}

function extractHostname(url) {
    var hostname;
    if (url.indexOf("//") > -1) {
        hostname = url.split('/')[2];
    } else {
        hostname = url.split('/')[0];
    }

    // find & remove port number hostname = hostname.split(':')[0]; find & remove
    // "?"
    hostname = hostname.split('?')[0];

    return hostname;
}

function _getDomain(bundle) {
    if (bundle.inputData.baseurl != null && bundle.inputData.baseurl.trim() != '') {
        return extractHostname(bundle.inputData.baseurl.trim());
    } else {
        return process.env.OAUTH_HOST;
    }
}

const _getSample = async function (z, bundle, type) {
    let getSampleUrl = '';
    switch (_checkPortalVersion(bundle.authData.domain)) {
        case 'autoCoding':
            getSampleUrl = `https://${bundle.authData.domain}/api/livechat/zapsample/${type}`;
            break;
        case 'xversion':
          let apiDomain = _getApiDomain(bundle.authData.domain)
            getSampleUrl = `https://${apiDomain}/api/v3/livechat/zapsamples/${type}`;
            break;
        case 'prex':
            break;
    }
    const options = {
        url: getSampleUrl,
        // url: `https://${bundle.authData.domain}/api/livechat/zapsample/${type}`,
        method: 'GET'
    };

    return z
        .request(options)
        .then((response) => {
            _checkResponse(z, response);
            return response.json;
        });
}

const _checkResponse = (z, response) => {
    if (response.status != 200) {
        //throw new Error(JSON.stringify(response));
        if (response.status == 401) {
            throw new z
                .errors
                .HaltedError('Access Denied: You have no permission for this operation.');
        } else if (response.json && response.json.Code && (response.json.Code == 4 || response.json.Code == 12 || response.json.Code == 13 || response.json.Code == 403003)) {
            throw new z
                .errors
                .HaltedError('Access Denied: You have no permission for this operation.');
        } else if (response.json && (response.json.Message || response.json.ErrorMessage)) {
            throw new z
                .errors
                .HaltedError((response.json.Message || response.json.ErrorMessage));
        } else {
            throw new z
                .errors
                .HaltedError(response.content);
        }
    }
}
const autoCodingPortalArray = new Array(
    'dash11.comm100.io',
    'dash12.comm100.io',
    'dash13.comm100.io',
    'dash15.comm100.io',
    'dash17.comm100.io',
    'autoportal.comm100dev.io',
    'dash11staging.comm100.io'
);
const xversionPortalArray = new Array(
    'portal1.comm100.io',
    'portal2.comm100.io',
    'portal3.comm100.io',
    'portal5.comm100.io',
    'portal7.comm100.io',
    'portal1.livelyhelp.chat',
    'portal.comm100dev.io'
);
const prexPortalArray = new Array(
    'hosted.comm100.com',
    'enterpriseportal.comm100.com',
    'ent1portal.comm100.com',
    'appportal.livelyhelp.chat',
    'hosted.comm100dev.com'
);

function _checkPortalVersion(domainUrl) {
    if (domainUrl.includes('testing.comm100dev.io')){
        return 'autoCoding';
    }
    for (var i in xversionPortalArray) {
        if (xversionPortalArray[i] == domainUrl) {
            return 'xversion';
        }
    }
    for (var i in autoCodingPortalArray) {
        if (autoCodingPortalArray[i] == domainUrl) {
            return 'autoCoding';
        }
    }
    for (var i in prexPortalArray) {
        if (prexPortalArray[i] == domainUrl) {
            return 'prex';
        }
    }
    return 'not matched';
}

const apiDomainDictionary = new Array();
apiDomainDictionary['portal1.comm100.io'] = 'api1.comm100.io';
apiDomainDictionary['portal2.comm100.io'] = 'api2.comm100.io';
apiDomainDictionary['portal3.comm100.io'] = 'api3.comm100.io';
apiDomainDictionary['portal5.comm100.io'] = 'api5.comm100.io';
apiDomainDictionary['portal7.comm100.io'] = 'api7.comm100.io';
apiDomainDictionary['portal1.livelyhelp.chat'] = 'api1.livelyhelp.chat';
apiDomainDictionary['portal.comm100dev.io'] = 'api.comm100dev.io';

function _getApiDomain(portalDomainUrl){
  return apiDomainDictionary[portalDomainUrl];
}

module.exports = {
    subscribe: _subscribeHook,
    unsubscribe: _unsubscribeHook,
    copyJsonObject: _copyJsonObject,
    checkUrl: _checkUrl,
    getSample: _getSample,
    getDomain: _getDomain,
    checkResponse: _checkResponse,
    postLog: _postLog,
    checkPortalVersion: _checkPortalVersion,
    getApiDomain:_getApiDomain
};
